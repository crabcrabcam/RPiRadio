import sys, random, os, subprocess
from sultan.api import Sultan
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow, QListWidgetItem, QListWidget, QMessageBox
from MainUI import Ui_MainWindow

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

	#Setup the class variables
	playerModel = QtGui.QStandardItemModel()
	playlist = []
	
	#Setup the window
	def __init__(self, parent=None):
		QtWidgets.QWidget.__init__(self, parent=parent)
		self.setupUi(self)
		
		#Player
		self.btnPlayerOpen.clicked.connect(self.openPlayerFiles)
		self.btnPlayerClear.clicked.connect(self.clearPlayerList)
		self.btnPlayerPlay.clicked.connect(self.playPlayer)
		
		
	#Opens the files for the player
	def openPlayerFiles(self):
		#Need the underscore apparently.
		options = QtWidgets.QFileDialog.Options()
		options |= QtWidgets.QFileDialog.DontUseNativeDialog
		files, _ = QtWidgets.QFileDialog.getOpenFileNames(self,"Open Files To Play", "/media/pi/","All Files (*);;Python Files (*.py)", options=options)
		
		#Set the list view to the model we've made
		self.listViewPlayer.setModel(self.playerModel)
		#Loop the files we have
		if files:
			for file in files:
				#Split the filename into just the last part (without the path)
				fileNameOnly = file.split("/")
				#Count the lenght
				nameCount = len(fileNameOnly)
				#Get the last part of the path (The name)
				fileNameLast = fileNameOnly[nameCount - 1]
				#Setup the playlist and list model
				item = QtGui.QStandardItem(fileNameLast)
				#Append the model (real list)
				self.playerModel.appendRow(item)
				#Append the playlist (List for omxplayer)
				self.playlist.append(file)
			
	#Clears the player list
	def clearPlayerList(self):
		#Just clear the model
		self.playerModel.clear()
		self.playlist = []
		
	#Plays the current playlist
	def playPlayer(self):
		print("Playing")
		#Define
		omxplayerScript = ""
		
		#Shuffle if needed
		if self.chkPlayerShuffle.isChecked():
			random.shuffle(self.playlist)
		
		#Loop
		for p in self.playlist:
			#Add quotes to each line (either that or forced to remove spaces... Can't do that)
			omxplayerScript += "/usr/bin/omxplayer" + " " "\"" + p + "\"" + "; "
			
		#HACK INC
		scriptFile = open("omxScript.sh", "w")
		scriptFile.write(omxplayerScript)
		scriptFile.close()
		
		#Define sultan (bash API)
		sultan = Sultan()
		#Make the file executable
		sultan.chmod("+x omxScript.sh")
		
		print(omxplayerScript)
		
		#Run the script we created
		sultan.lxterminal("-e", "./omxScript.sh").run()

		
#Launch the main section of the app
if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)
	window = MainWindow()
	window.show()
	sys.exit(app.exec_())
